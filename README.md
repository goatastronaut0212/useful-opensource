# useful-opensource

Here is a list contain useful open source software used by me

## Editor

+ Neovim: terminal editor written and config in Lua.

## Operating System

+ NetBSD: a stable and portable BSD operating system.
+ NixOS: a stable and reproduce Linux distro with Nix package manager, no
conflict packages, highly flexible. But you may want to learn Nix.

## Terminal Emulator

+ foot: a lightweight Wayland terminal emulator.

## Web browser

+ qutebrowser: a web browser have keybinding like Vim written in Python and
QtWebEngine
